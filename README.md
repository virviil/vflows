# vflows

# Formatting

```
$ poetry run black .
```

# Linting 

```
$ poetry run flake8
$ poetry run mypy
$ poetry run pylint
```

# Example usage

This is fuully async library for asyncio.
Main entry point to the library - is context manager:

```python
from vflows.context_manager import VFlowsContextManager
```

Initialization is done with all the configuration for models:

```python
from vflows.models_loader import load_models
```

and `base host`, which is a template string that can be formatted then:

```python
base_host = "vmr-network-app-{}.staging.syte.app:443"
```

So, to iniitialize `context manager`:

```python
context_manager = VFlowsContextManager(base_host, load_models(Path("/path/to/vmr_network_data/models")))
```

In order to use it:

```python
async def use_context_manager():
    async with context_manager:
        results = await context_manager.process("model_name", "flow_name", **input_data)
```

Here model name and flow name are names of the model and flow that will be used. Can be passed per request.
Input data depends on what model and flow you are using.

# Configuring logger

Root logger here is `"vflows"`.

To get it, use:

```python
import logging

logger = logging.getLogger("vflows")
```

Setting it up is pure straightforward:

```python
# create logger
logger = logging.getLogger("vflows")
logger.setLevel(logging.DEBUG)

# create console handler and set level to debug
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)

formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)

logger.addHandler(ch)
```