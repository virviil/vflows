from typing import Dict

from vflows.flows import BaseFlow, VFlowsFactory
from vflows.requirements import Requirements
import sytedt.inferer as dt


class VFlowsContextManager:
    """
    Async context manager for VFlows.
    It uses dependency injection with factories thet creates needed statefull requirements
    for models
    """

    def __init__(
        self,
        models_index: Dict[str, Dict[str, dict]],
        requirements: Requirements,
    ):
        vflows_factory = VFlowsFactory()

        self.__models_index: Dict[str, Dict[str, BaseFlow]] = {}
        self.__requirements = requirements

        for model_name, model_config in models_index.items():
            specific_model_index = {}

            model_base_path = model_config["base_path"]
            model_flows_config = model_config["flows"]

            for flow_name, flow_config in model_flows_config.items():
                flow_type = flow_config["flow_type"]

                flow_instance: BaseFlow = vflows_factory.get_flow(
                    flow_type, model_base_path, flow_config
                )
                self.__requirements.append(flow_instance.get_requirements())

                # Adding instance to the index
                specific_model_index[flow_name] = flow_instance

            # Populating full models index
            self.__models_index[model_name] = specific_model_index

    def show_requirements(self):
        return self.__requirements

    def show_models(self):
        return self.__models_index

    # Async part
    async def __aenter__(self):
        await self.__requirements.__aenter__()
        return self

    async def __aexit__(self, exc_type, exc_val, exc_tb):
        await self.__requirements.__aexit__(exc_type, exc_val, exc_tb)

    # Sync part
    def __enter__(self):
        self.__requirements.__enter__()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.__requirements.__exit__(exc_type, exc_val, exc_tb)

    async def process(self, model_name: str, flow_name: str, *args, **kwargs):
        flow_instance = self.__models_index[model_name][flow_name]
        return await flow_instance.process(self.__requirements, *args, **kwargs)

    async def infer(
        self,
        model_name: str,
        flow_name: str,
        input: dt.Input,
        process_params: dict = None,
    ) -> dt.Prediction:
        if process_params is None:
            process_params = {}
        flow_instance = self.__models_index[model_name][flow_name]
        return await flow_instance.infer(self.__requirements, input, process_params)
