import json
from pathlib import Path


def load_models(base_path: Path):
    return {
        model_path.name: {
            "base_path": model_path,
            "flows": json.loads(model_path.joinpath("config.json").read_text()),
        }
        for model_path in base_path.iterdir()
        if model_path.is_dir()
    }
