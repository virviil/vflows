import asyncio
from typing import List, Dict
from sytedt.client import InferClient, InferClientFactory


class Requirements:
    def __init__(self, client_factory: InferClientFactory):
        self.__client_factory = client_factory
        self.networks: List[str] = []
        self.clients: Dict[str, InferClient] = {}

    def append(self, another_requirements: List[str]):
        self.networks.extend(another_requirements)

    async def __aenter__(self):
        for network in self.networks:
            self.clients[network] = self.__client_factory.get_client(network)

        await asyncio.gather(*[client.__aenter__() for client in self.clients.values()])
        return self

    async def __aexit__(self, exc_type, exc_val, exc_tb):
        await asyncio.gather(
            *[
                client.__aexit__(exc_type, exc_val, exc_tb)
                for client in self.clients.values()
            ]
        )
