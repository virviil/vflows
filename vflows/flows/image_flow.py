import json
import logging
import copy
from typing import Dict, Any, List
from pathlib import Path

from vflows.requirements import Requirements
from sytedt.client import InferClient
from vflows.flows.base_flow import BaseFlow
from vflows.utils import consts

from sytedt import inferer as dt


logger = logging.getLogger(__name__)


class ImageFlow(BaseFlow):
    def __init__(self, base_path: Path, config: dict):
        super().__init__(base_path, config)
        self.cbir_networks: Dict[str, str] = {
            "home": next(iter(config["cbir_home_sid"])),
            "fashion": next(iter(config["cbir_fashion_sid"])),
            "jewelry": next(iter(config["cbir_jewelry_sid"])),
        }

        with open(
            base_path / config["cropper_categories"],
            "rt",
            encoding="utf-8",
        ) as f:
            self.cropper_categories = json.load(f)
            logger.info("loaded cropper_categories File")

        with open(base_path / config["bb_to_cropper"], "rt", encoding="utf-8") as f:
            self.bb_to_cropper = json.load(f)
            logger.info("loaded bb_to_cropper File")

        with open(
            base_path / config["cbir_network_selector"],
            "rt",
            encoding="utf-8",
        ) as f:
            self.cbir_network_selector = json.load(f)
            logger.info("loaded cbir_network_selector File")
        self.cbir_filtering = self.connect_filters(
            self.cropper_categories, self.bb_to_cropper
        )

    @staticmethod
    def connect_filters(cropper_att: dict, bb2cropper: dict) -> dict:
        final_dict: dict = {}
        for bb, category in bb2cropper.items():
            for bb_cat in category:
                found = False
                for cropper_cat in cropper_att:
                    if cropper_cat["name"] == bb_cat:
                        found = True
                        if bb in final_dict:
                            for att, list_val in cropper_cat["attributes"].items():
                                if att not in final_dict[bb]:
                                    final_dict[bb][att] = copy.deepcopy(list_val)
                                else:
                                    for value in list_val:
                                        final_dict[bb][att].append(value)
                                    final_dict[bb][att] = list(set(final_dict[bb][att]))
                        else:
                            final_dict[bb] = copy.deepcopy(cropper_cat["attributes"])

                        break
                if not found:
                    logger.error(
                        "No translation of category was found for %r, %r", bb_cat, bb
                    )
        return final_dict

    def filter_cbir_tags(self, deep_tags: List[dict], detection: str) -> list:
        filtered_res = []
        for deep_tag in deep_tags:
            if deep_tag["name"] in self.cbir_filtering[detection]:
                if (
                    deep_tag["value"]
                    in self.cbir_filtering[detection][deep_tag["name"]]
                ):
                    filtered_res.append(deep_tag)
        return filtered_res

    async def get_cbir(
        self, requirements: Requirements, bounds: list, image_source: bytes
    ) -> list:
        img_res = []
        for bound in bounds:
            if bound["catalog"] == consts.PERSON:
                continue
            cbir_name = self.cbir_network_selector[bound["labels"]]
            cbir_net: InferClient = requirements.clients[self.cbir_networks[cbir_name]]
            cbir_results: Any = (
                await cbir_net.predict(
                    dt.Input(
                        entities=[
                            dt.InputEntity(
                                data=image_source,
                                options=json.dumps(
                                    {"bounds": [bound[consts.BOX]]}
                                ).encode("utf8"),
                            )
                        ]
                    )
                )
            ).entities[0]
            bound["cbir_name"] = cbir_name
            # TODO
            signature_vector = cbir_results["vector"]
            img_res.append(
                {
                    "signature_vector": signature_vector,
                    "raw_cbir": json.loads(cbir_results.mapping["deeptags"].data),
                    "deep_tags": self.filter_cbir_tags(cbir_results, bound["labels"]),
                    "bb_results": bound["labels"],
                }
            )

        return img_res
