import orjson
from typing import Dict, Any
from pathlib import Path
from vflows.flows.base_flow import BaseFlow
from vflows.requirements import Requirements
from vflows.utils import tagit_consts
from sytedt.client import InferClient


class TagitMl(BaseFlow):
    def __init__(self, base_path: Path, config: Dict[str, Any]):
        super().__init__(base_path, config)
        self.tagit_network = list(config["tagit_sid"].keys())[0]

    def get_requirements(self):
        return self.tagit_network

    async def infer(
        self,
        requirements: Requirements,
        input,
        process_params: dict,
    ):
        raise NotImplementedError

    async def process(
        self,
        requirements: Requirements,
        _image_source: dict,
        item_data: dict,
        process_params: dict = None,
    ) -> tuple:
        if process_params is None:
            process_params = {}
            ret_col = tagit_consts.OUTPUT_I2BB_FIELD
        elif "return_col" in process_params:
            ret_col = process_params["return_col"]
        else:
            ret_col = tagit_consts.OUTPUT_I2BB_FIELD
        tagit_client: InferClient = requirements.clients[self.tagit_network]
        data_to_model = {
            "products": [
                {
                    "categories": item_data["categories"],
                    "sku": item_data["sku"],
                    "title": item_data["title"],
                }
            ]
        }
        results = await tagit_client.predict(data_to_model)
        # Model classification
        return orjson.loads(results.entities[0].data)[ret_col]
