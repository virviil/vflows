import orjson
from typing import Dict, Any, Tuple
import time
import logging
from pathlib import Path
from vflows.flows.base_flow import BaseFlow
from vflows.utils import consts, flows
from vflows.requirements import Requirements
from sytedt.client import InferClient
from sytedt.inferer import Input, InputEntity, Prediction, PredictionEntity

logger = logging.getLogger(__name__)


class Nalli(BaseFlow):
    def __init__(self, base_path: Path, config: Dict[str, Any]):
        super().__init__(base_path, config)
        self.cbir_network = next(iter(config["cbir_fashion_sid"]))

    def get_requirements(self):
        return [self.cbir_network]

    @staticmethod
    def filter_nalli_tags(deep_tags: dict) -> list:
        filtered_res = []
        for deep_tag in deep_tags:
            if deep_tag["name"] in ["Color"]:
                filtered_res.append(deep_tag)
        return filtered_res

    async def get_dt(
        self, cbir_client: InferClient, img_bin: bytes, coords: list
    ) -> Tuple[dict, bytes]:
        cbir_prediction, _details = await cbir_client.predict(
            Input(entities=[InputEntity(data=img_bin, options={"bounds": [coords]})])
        )
        cbir_results = cbir_prediction.entities[0].mapping
        category: str = cbir_results["category"].data.decode("utf-8")
        vector: bytes = cbir_results["vector"].data
        raw_deeptags: dict = orjson.loads(cbir_results["deeptags"].data)
        return (
            {
                "cbir_category": category,
                "raw_cbir": raw_deeptags,
                "deep_tags": self.filter_nalli_tags(raw_deeptags),
                "filtered_BB": "Dress",
            },
            vector,
        )

    async def get_dt_updated(
        self, cbir_client, suffix: str, bounding_box: list, image_data: Dict[str, Any]
    ) -> Tuple[dict, bytes]:
        raw_dt, vector = await self.get_dt(
            cbir_client, image_data["image_bin"], bounding_box
        )
        raw_dt["bb_results"] = suffix
        raw_dt["bb_rec"] = []
        raw_dt.update(image_data["image_json"])
        return raw_dt, vector

    async def find_color(
        self, requirements: Requirements, image_dict: dict
    ) -> Tuple[list, Dict[str, Any], str, bytes]:
        flow_update: Dict[str, Any] = {
            "BB selection flow": "Nalli",
            "CTL bounds num": 0,
            "ctl_image": {},
        }
        image_to_index = ""
        ret_signature_vec: bytes = b""

        cbir_client: InferClient = requirements.clients[self.cbir_network]
        nalli_mapping = {
            "m": [0.35, 0.3, 0.45, 0.5],
            "pp": [0.01, 0.01, 0.2, 0.3],
            "pb": [0.3, 0.01, 0.7, 0.3],
            "b": [0.3, 0.3, 0.5, 0.5],
            "bo": [0.01, 0.01, 0.25, 0.7],
            "c": [0.1, 0.1, 0.9, 0.9],
        }
        call_data = {}
        found = False
        flow_update["Cbir Calls"] = 0

        raw_results = []
        for image_url, image_data in image_dict.items():
            if image_url.endswith(".jpg"):
                suffix = image_url.split(".jpg")[0].split("_")[-1]
                if suffix in nalli_mapping:
                    call_data[suffix] = image_data

        for image_type in ["m", "c", "pp", "pb", "bo", "b"]:
            if image_type in call_data:
                raw_dt, vector = await self.get_dt_updated(
                    cbir_client,
                    image_type,
                    nalli_mapping[image_type],
                    call_data[image_type],
                )
                flow_update["Cbir Calls"] += 1
                if len(raw_dt["deep_tags"]) > 0:
                    ret_signature_vec = vector
                    found = True
                raw_results.append([raw_dt])
                if found:
                    break

        return raw_results, flow_update, image_to_index, ret_signature_vec

    async def process(
        self,
        requirements: Requirements,
        image_source: dict,
        item_data: dict,
        process_params: dict = None,
    ) -> Tuple[dict, bytes]:
        result: Prediction = await self.infer(
            requirements=requirements,
            input=Input(
                entities=[
                    InputEntity(
                        mapping={
                            image_url: InputEntity(data=image)
                            for image_url, image in image_source.items()
                        },
                        options=item_data,
                    )
                ]
            ),
            process_params=(process_params or {}),
        )
        return result.entities[0].details, result.entities[0].data

    async def infer(
        self,
        requirements: Requirements,
        input: Input,
        process_params: dict,
    ) -> Prediction:
        image_source = {
            key: entity.data for key, entity in input.entities[0].mapping.items()
        }
        item_data = input.entities[0].options
        start = time.time()
        logger.info("started SKU %r ", item_data["sku"])
        image_dict, bad_image_urls = flows.preprocess_inputs(
            image_source, 100000, False, 50
        )
        feed_analysis = {
            "Selected bound": "Dresses",
            "Bounds bank": [],
            "BB selection flow": "All Images Urls are invalid",
            "bad_images_urls": bad_image_urls,
            consts.UNIQUE_BOUNDS_COUNT: {},
            "deep_tags_len": 0,
            "images_num": len(image_dict),
            "CTL bounds num": 0,
            "Cbir Calls": 0,
            "BB Calls": 0,
        }
        logger.info("Checked Inputs Duration:  %r sec ", time.time() - start)
        if len(image_dict) == 0:
            image_json = {}
            for image_url, image_dict in image_dict.items():
                image_json[image_url] = image_dict.get("image_json", {})
            feed_analysis["duration"] = time.time() - start

            ret_dict = flows.set_return_obj(
                ctl_image={},
                tagging_json=[],
                tagging={},
                lexicon_tagging={},
                debug_vmr=image_json,
                bounds=[],
                image_to_index="",
                bb_categories=[],
                feed_analysis=feed_analysis,
                tags=[],
            )

            return Prediction(
                entities=[PredictionEntity(data=b"", details=ret_dict)]  # empty vector
            )
        (
            raw_results,
            flow_update,
            image_to_index,
            ret_signature_vec,
        ) = await self.find_color(requirements, image_dict)
        feed_analysis.update(flow_update)
        deep_tags_prob = flows.set_tagging_json(raw_results)
        tagging = flows.set_tagging(deep_tags_prob)
        feed_analysis["duration"] = time.time() - start
        feed_analysis["deep_tags_len"] = len(tagging)
        ret_dict = flows.set_return_obj(
            ctl_image=image_to_index,
            tagging_json=list(deep_tags_prob.values()),
            tagging=tagging,
            lexicon_tagging=tagging,
            debug_vmr=raw_results,
            bounds=[],
            image_to_index=image_to_index,
            bb_categories=[],
            feed_analysis=feed_analysis,
            tags=[],
        )

        end = time.time()
        logger.info("process ended duration:  %r sec", end - start)

        return Prediction(
            entities=[PredictionEntity(data=ret_signature_vec, details=ret_dict)]
        )
