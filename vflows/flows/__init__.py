import importlib
from .base_flow import BaseFlow  # noqa: F401
from .image2dt import Images2Dt  # noqa: F401
from .nalli import Nalli  # noqa: F401
from .bb import BB  # noqa: F401
from .rawdt import RawDT  # noqa: F401
from .vectorizer import Vectorizer  # noqa: F401
from .lexicons import Lexicons  # noqa: F401
from .tagit_ml import TagitMl  # noqa: F401


class VFlowsFactory:
    def __init__(self):
        self.__loader = importlib.import_module("vflows.flows")

    def get_flow(self, flow_name: str, model_base_path, flow_config):
        return getattr(self.__loader, flow_name)(model_base_path, flow_config)
