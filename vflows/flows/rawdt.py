import logging
import asyncio
from pathlib import Path
from vflows.flows.lexicons import Lexicons
from vflows.flows.image_flow import ImageFlow
from vflows.requirements import Requirements
from sytedt.client import InferClient
from vflows.utils import consts, flows

logger = logging.getLogger(__name__)


class RawDT(ImageFlow):
    def __init__(self, base_path: Path, config: dict):
        super().__init__(base_path, config)
        self.bb_fashion = next(iter(config["yolo_fashion_sid"]))
        self.bb_home = next(iter(config["yolo_home_sid"]))
        self.bb_person = next(iter(config["yolo_person_sid"]))
        self.lexicons = Lexicons(
            base_path / config["lexicons_path"], self.bb_to_cropper
        )

    def get_requirements(self):
        return [
            self.bb_fashion,
            self.bb_home,
            *self.cbir_networks.values(),
        ]

    async def get_bounds(self, requirements: Requirements, image_bin: bytes) -> dict:
        bb_image_bin = flows.resize_image(image_bin)
        bb_fashion: InferClient = requirements.clients[self.bb_fashion]
        bb_home: InferClient = requirements.clients[self.bb_home]
        bb_person: InferClient = requirements.clients[self.bb_person]

        bb_results = await asyncio.gather(
            *[
                bb_fashion.predict(bb_image_bin),
                bb_home.predict(bb_image_bin),
                bb_person.predict(bb_image_bin),
            ]
        )
        for bound in bb_results[0]:
            bound["catalog"] = consts.FASHION
        for bound in bb_results[1]:
            bound["catalog"] = consts.HOME
        for bound in bb_results[2]:
            bound["catalog"] = consts.PERSON
        return [*bb_results[0], *bb_results[1], *bb_results[2]]

    async def infer(
        self,
        requirements: Requirements,
        input,
        process_params: dict,
    ):
        raise NotImplementedError

    async def process(
        self,
        requirements: Requirements,
        image_source: bytes,
        _item_data: dict = None,
        process_params: dict = {},
    ) -> tuple:
        """

        :param requirements: requirements to have all IO for this flow
        :param image_source:
        :param item_data: All the data, awailable for the parameter. Contains:
            - sku - item sku
            - tags - item tags

        """
        bounds = await self.get_bounds(requirements, image_source)
        img_res = await self.get_cbir(requirements, bounds, image_source)
        if "Lexicon" in process_params:
            lexicon_name = process_params["Lexicon"]
        else:
            lexicon_name = "syte_rba"
        logger.info("use lexicon %r", lexicon_name)
        for img in img_res:

            img["tagging_json"] = flows.set_tagging_json(img["deep_tags"])
            img["tagging"] = flows.set_tagging(img["tagging_json"])
            img["tagging"]["Cat"] = [img["bb_results"]]
            logger.info("bb results %r", img["bb_results"])
            img["lexicon_tagging"], _, _ = self.lexicons.process(
                None,
                None,
                {"tagging": img["tagging"], "bbCategory": img["bb_results"]},
                {"lexicon_name": lexicon_name},
            )
        return img_res
