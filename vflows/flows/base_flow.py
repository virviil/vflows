from abc import ABC, abstractmethod
from pathlib import Path
from typing import Any, Dict, List

from vflows.requirements import Requirements
from sytedt.inferer import Input, Prediction


class BaseFlow(ABC):
    @abstractmethod
    def __init__(self, base_model_path: Path, config: Dict[str, Any]):
        ...

    @abstractmethod
    def get_requirements(self) -> List[str]:
        ...

    @abstractmethod
    async def process(
        self,
        requirements: Requirements,
        image_source: dict,
        item_data: dict,
        process_params: dict,
    ) -> Any:
        ...

    @abstractmethod
    async def infer(
        self,
        requirements: Requirements,
        input: Input,
        process_params: dict,
    ) -> Prediction:
        ...
