import logging
from pathlib import Path
from typing import Dict, Any

from vflows.flows.image_flow import ImageFlow
from vflows.requirements import Requirements

logger = logging.getLogger(__name__)


class Vectorizer(ImageFlow):
    def __init__(self, base_path: Path, config: Dict[str, Any]):
        super().__init__(base_path, config)

    def get_requirements(self):
        return [*self.cbir_networks.values()]

    async def infer(
        self,
        requirements: Requirements,
        input,
        process_params: dict,
    ):
        raise NotImplementedError

    async def process(
        self,
        requirements: Requirements,
        image_source: bytes,
        item_data: dict,
        _process_params: dict = {},
    ) -> Any:
        return await self.get_cbir(requirements, item_data["bounds"], image_source)
