import json
import logging
from pathlib import Path
import time
from typing import Dict, Any
import pandas as pd
from vflows.requirements import Requirements


logger = logging.getLogger(__name__)

LEXICON = "Lexicon"
RBA = "RBA"
ATT = "Attribute"
VAL = "Value"


def init_dict(key: str, dictionary: dict):
    if key not in dictionary:
        dictionary[key] = {}


def append_rule(rule, ret_dict) -> int:
    if rule["output"][ATT] not in ret_dict:
        ret_dict[rule["output"][ATT]] = []
    if rule["output"][VAL] not in ret_dict[rule["output"][ATT]]:
        ret_dict[rule["output"][ATT]].append(rule["output"][VAL])
        return 1
    return 0


def read_ext(filename: str, sheet: str, return_dict):
    extension_tab = pd.read_excel(filename, sheet_name=sheet).to_dict()
    return_dict[sheet] = extension_tab
    logger.info("working on rules %r", sheet)
    _, category, att = sheet.split(".")
    cat_ext = {}
    for ext_val, ext_cond in extension_tab.items():
        cat_ext[ext_val] = []
        for conditions in ext_cond.values():
            if isinstance(conditions, float):
                continue
            split_cond = conditions.split("^")
            cond_list = []
            for item in split_cond:

                att_value = item.split(".")
                cond_list.append({ATT: att_value[0], VAL: att_value[1]})

            cat_ext[ext_val].append(cond_list)
    if category not in return_dict[RBA]:
        return_dict[RBA][category] = {}
    return_dict[RBA][category][att] = cat_ext.copy()


def read_lexicon(filename: str) -> Dict[str, dict]:
    return_dict = {RBA: {}}
    xls_fashion = pd.ExcelFile(filename, engine="openpyxl")
    for sheet in xls_fashion.sheet_names:
        if "Ext" in sheet:
            read_ext(filename, sheet, return_dict)
        elif LEXICON in sheet:
            trans_df = pd.read_excel(filename, sheet_name=sheet)
            trans_dict = trans_df.fillna("").to_dict("records")
            return_dict[LEXICON] = trans_dict
    return return_dict


class Singleton(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


class Lexicons(metaclass=Singleton):
    def __init__(self, base_path: Path, config: dict):
        super().__init__()
        with open(
            base_path / config["bb_to_cropper"],
            "rt",
            encoding="utf-8",
        ) as f:
            self.bb2cropper = json.load(f)
            logger.info("loaded cropper_categories File")
        self.lexicons = {}
        for filename in (base_path / config["lexicons_path"]).glob("**/*.xlsx"):
            lex_name = filename.stem
            logger.info("working on %r", lex_name)
            lexicon = read_lexicon(filename)
            new_lexicon = {
                LEXICON: self.convert_to_dict(lexicon[LEXICON]),
                RBA: self.reverse_index(lexicon[RBA]),
            }

            self.lexicons[lex_name] = new_lexicon
        logger.info(" loaded the following lexicons %r", list(self.lexicons.keys()))

    def get_requirements(self):
        return []

    @staticmethod
    def ext_deeptags(
        deeptags: Dict[str, Any], rba: dict, category: str
    ) -> Dict[str, Any]:
        ret_dict = {}
        val_num = 0
        if category not in rba:
            logger.error("missing rba category %r", category)
            return ret_dict, val_num
        for key, list_of_values in deeptags.items():
            if key not in rba[category]:
                continue
            for value in list_of_values:
                if value not in rba[category][key]:
                    continue
                rules = rba[category][key][value]
                for rules_list in rules.values():
                    for rule in rules_list:
                        if len(rule["cond"]) == 1:
                            val_num += append_rule(rule, ret_dict)
                        else:
                            add = True
                            for con in rule["cond"]:
                                if (
                                    con[ATT] in deeptags
                                    and con[VAL] in deeptags[con[ATT]]
                                ):
                                    continue
                                add = False
                            if add:
                                val_num += append_rule(rule, ret_dict)

        return ret_dict, val_num

    @staticmethod
    def lexicon_deeptags(
        deeptags: Dict[str, Any], lexicon: dict, category: str
    ) -> Dict[str, Any]:
        ret_dict = {}
        if category not in lexicon:
            logger.error("missing category %r", category)
            return ret_dict
        for key, list_of_values in deeptags.items():
            if key not in lexicon[category]:
                logger.error("missing key %r for  category %r", key, category)
                continue
            for value in list_of_values:
                if value not in lexicon[category][key]:
                    logger.error(
                        "missing value %r for  category %r, att %r",
                        value,
                        category,
                        key,
                    )
                    continue

                lex = lexicon[category][key][value]
                if len(lex) == 0:
                    continue
                if lex[ATT] in ret_dict:
                    ret_dict[lex[ATT]].append(lex[VAL])
                else:
                    ret_dict[lex[ATT]] = [lex[VAL]]
        return ret_dict

    async def infer(
        self,
        requirements: Requirements,
        input,
        process_params: dict,
    ):
        raise NotImplementedError

    def process(
        self,
        _requirements: Requirements,
        _image_source: dict,
        item_data: Dict[str, Any],
        process_params: dict = {},
    ) -> tuple:
        # def process_deeptags(
        #    self, deeptags: Dict[str, Any], bb_label: str, lexicon_name: str
        # ) -> Dict[str, Any]:
        process_params = (
            {"lexicon_name": "syte_rba"}
            if "lexicon_name" not in process_params
            else process_params
        )
        if process_params["lexicon_name"] not in self.lexicons:
            logger.error(
                "lexicon not found, no translation is done, returning original values"
            )
            return item_data["tagging"]
        category = self.bb2cropper[item_data["bbCategory"]][0]
        lex_dict = self.lexicon_deeptags(
            item_data["tagging"],
            self.lexicons[process_params["lexicon_name"]][LEXICON],
            category,
        )
        lexicon_dt = len(lex_dict)
        ext_dict, thematic_num = self.ext_deeptags(
            item_data["tagging"],
            self.lexicons[process_params["lexicon_name"]][RBA],
            category,
        )
        lex_dict.update(ext_dict)

        return lex_dict, thematic_num, lexicon_dt

    @staticmethod
    def convert_to_dict(lexicon: list) -> dict:
        conv_dict = {}
        for row in lexicon:
            init_dict(row["CategoryName"], conv_dict)
            init_dict(row[ATT], conv_dict[row["CategoryName"]])
            if row["Action"] == "Keep":
                conv_dict[row["CategoryName"]][row[ATT]][row[VAL]] = {
                    ATT: row[ATT],
                    VAL: row[VAL],
                }
            elif row["Action"] == "Move":
                conv_dict[row["CategoryName"]][row[ATT]][row[VAL]] = {
                    ATT: row["ClientAttribute"],
                    VAL: row["ClientValue"],
                }
            else:
                conv_dict[row["CategoryName"]][row[ATT]][row[VAL]] = {}

        return conv_dict

    @staticmethod
    def reverse_index(extensions: dict) -> dict:
        # logic covered in one internal function
        def update_rev_dict(cond, reverse_dict, cat, ext_att, ext_val):
            for terms in cond:
                init_dict(terms[ATT], reverse_dict[cat])
                init_dict(terms[VAL], reverse_dict[cat][terms[ATT]])
                att_val_d = reverse_dict[cat][terms[ATT]][terms[VAL]]
                # option one => check all rules - not efficient but simple
                # option two => per value stack all conditions
                rule_name = f"{ext_att}_{ext_val}"
                if rule_name not in att_val_d:
                    att_val_d[rule_name] = []
                if len([rule_name]) > 0:
                    # check if this cond exist:
                    for item in att_val_d[rule_name]:
                        if item["cond"] == cond and item["output"] == {
                            ATT: ext_att,
                            VAL: ext_val,
                        }:
                            continue
                att_val_d[rule_name].append(
                    {
                        "cond": cond,
                        "output": {ATT: ext_att, VAL: ext_val},
                    }
                )

        # reverse the index
        reverse_dict = {}
        for cat in extensions:
            init_dict(cat, reverse_dict)
            for ext_att in extensions[cat]:
                for ext_val in extensions[cat][ext_att]:
                    for cond in extensions[cat][ext_att][ext_val]:
                        update_rev_dict(cond, reverse_dict, cat, ext_att, ext_val)
        return reverse_dict


def main():

    with open(
        "./tests/resources/bb2cropper_categories.json", "rt", encoding="utf-8"
    ) as f:
        bb_to_cropper = json.load(f)
        logger.info("loaded bb_to_cropper File")
    lex = Lexicons("./tests/resources/", bb_to_cropper)
    with open("./tests/resources/test_image2dt.json", "rt", encoding="utf-8") as f:
        items = json.load(f)
    final_list = []
    start = time.time()
    i = 0
    for item in items:
        if len(item["_source"]["tagging"]) == 0:
            continue
        i += 1
        if len(item["_source"]["bbCategories"]) > 0:
            bb = item["_source"]["bbCategories"][0]
        else:
            bb = "AnyOtherObject"
        final_list.append(
            {
                "In": item["_source"]["tagging"],
                "Out": lex.process_deeptags(
                    item["_source"]["tagging"],
                    bb,
                    "syte_rba",
                ),
            }
        )
    end = time.time()
    logger.error(
        " time for running %r item, %r sec, avg: %r",
        len(items),
        end - start,
        (end - start) / i,
    )
    with open("stam.json", "wt", encoding="utf-8") as f:
        json.dump(final_list, f)


if __name__ == "__main__":
    main()
