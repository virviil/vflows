import json
import orjson
import logging
import time
from pathlib import Path
from typing import Any, Dict, Tuple

import numpy as np

from vflows.flows.image_flow import ImageFlow
from vflows.flows.lexicons import Lexicons
from vflows.requirements import Requirements
from sytedt.client import InferClient
from vflows.utils import consts, flows, tagit_consts

from sytedt.inferer import Input, InputEntity, Prediction, PredictionEntity


logger = logging.getLogger(__name__)


def fix_tags(bounds: list, ctl_image: Dict[str, Any], tags: list) -> None:
    for bound in bounds:
        bound["tags"] = tags
    if "bounds" in ctl_image:
        for bound in ctl_image["bounds"]:
            bound["tags"] = tags


class Images2Dt(ImageFlow):
    def __init__(self, base_path: Path, config: dict):
        super().__init__(base_path, config)
        self.lexicons = Lexicons(base_path, config)
        self.bb_fashion = next(iter(config["yolo_fashion_sid"]))
        self.bb_home = next(iter(config["yolo_home_sid"]))
        self.tagit_network = next(iter(config["tagit_sid"]))
        with open(base_path / config["label2bb"], "rt", encoding="utf-8") as f:
            self.label2bb = json.load(f)
            logger.info("loaded label2bb File")

        with open(base_path / config["bb2label"], "rt", encoding="utf-8") as f:
            self.bb2label = json.load(f)
            logger.info("loaded bb2label File")

        with open(base_path / config["similarbbs"], "rt", encoding="utf-8") as f:
            self.similarbbs = json.load(f)
            logger.info("loaded similarbbs File")
        self.label2similars = self.create_label2similars(self.label2bb, self.similarbbs)

        with open(
            base_path / config["bb_fashion_categories"],
            "rt",
            encoding="utf-8",
        ) as f:
            self.fashion_cat = [line.rstrip() for line in f]

        with open(
            base_path / config["bb_home_categories"],
            "rt",
            encoding="utf-8",
        ) as f:
            self.home_cat = [line.rstrip() for line in f]

    def get_requirements(self):
        return [
            self.bb_fashion,
            self.bb_home,
            self.tagit_network,
            *self.cbir_networks.values(),
        ]

    def set_bb_return_obj(
        self,
        bound: str,
        bb_selection_flow: str,
        bound_bank: list,
        unique_bounds_count: dict,
        tagit_results: dict = None,
    ) -> tuple[list, dict, str]:

        if bound == "":
            bounds = []
            optional_bb = []
        else:
            bounds = self.label2similars[bound]
            optional_bb = self.label2similars[bound]
        return (
            bounds,
            {
                consts.BB_SELECTION_FLOW: bb_selection_flow,
                consts.SELECTED_BOUND: bound,
                consts.BOUNDS_BANK: bound_bank,
                consts.OPTIONAL_BBS: optional_bb,
                consts.UNIQUE_BOUNDS_COUNT: unique_bounds_count,
                consts.TAGIT: tagit_results if tagit_results else {},
            },
            bound,
        )

    @staticmethod
    def create_label2similars(label2bb, similarsbb) -> dict:
        label2similars = {}
        for label, bbs in label2bb.items():
            temp_set = set(bbs)
            for item in bbs:
                if item in similarsbb:
                    temp_set.update(similarsbb[item])
            label2similars[label] = list(temp_set)
        return label2similars

    @staticmethod
    def set_bounds(
        bounds: list,
        uniq_bounds: set,
        image_json: dict,
        catalog: str,
        tags: list,
        gender: str,
    ) -> None:
        for cur_bound in bounds:
            dict_obj = cur_bound
            dict_obj["catalog"] = catalog
            # REMOVE ME after matan fix the code.
            dict_obj["label"] = dict_obj["labels"]

            dict_obj["tags"] = tags
            dict_obj["gender"] = gender
            dict_obj["x0"] = dict_obj["box"][0]
            dict_obj["y0"] = dict_obj["box"][1]
            dict_obj["x1"] = dict_obj["box"][2]
            dict_obj["y1"] = dict_obj["box"][3]
            dict_obj["scaled_x0"] = dict_obj["box"][0]
            dict_obj["scaled_y0"] = dict_obj["box"][1]
            dict_obj["scaled_x1"] = dict_obj["box"][2]
            dict_obj["scaled_y1"] = dict_obj["box"][3]
            dict_obj["image_url"] = image_json["image_url"]
            dict_obj["image_size"] = image_json["image_size"]
            dict_obj["bb_size"] = image_json["bb_size"]

            uniq_bounds.add(dict_obj["label"])
            image_json["bb_rec"].append(dict_obj)

    async def get_bounds(
        self,
        requirements: Requirements,
        images_dict: dict,
        item_data: dict,
        catalog: list,
    ) -> tuple[dict, int, list]:
        bb_calls = 0
        if not catalog:
            catalog = [consts.FASHION, consts.HOME]
        bb_net = set("")
        unique_bounds_count: Dict[str, int] = {}
        # start = time.time()
        for image_dict in images_dict.values():
            try:
                image_json = image_dict["image_json"]
                uniq_bounds = set("")
                image_json["bb_rec"] = []
                bb_input = Input(entities=[InputEntity(data=image_dict["image_bb"])])
                if consts.FASHION in catalog:
                    fashion_bb: InferClient = requirements.clients[self.bb_fashion]
                    print("Fashion BB clien is: ", fashion_bb)
                    f_prediction, details = await fashion_bb.predict(bb_input)
                    f_bounds = [
                        json.loads(entity.data)
                        for entity in f_prediction.entities[0].listing
                    ]
                    self.set_bounds(
                        f_bounds,
                        uniq_bounds,
                        image_json,
                        consts.FASHION,
                        item_data["tags"],
                        item_data["gender"],
                    )
                    bb_calls += 1
                    bb_net.add(self.bb_fashion)
                else:
                    f_bounds = []
                if consts.HOME in catalog:
                    home_bb: InferClient = requirements.clients[self.bb_home]
                    h_prediction, _details = await home_bb.predict(bb_input)
                    h_bounds = [
                        json.loads(entity.data)
                        for entity in h_prediction.entities[0].listing
                    ]
                    self.set_bounds(
                        h_bounds,
                        uniq_bounds,
                        image_json,
                        consts.HOME,
                        item_data["tags"],
                        item_data["gender"],
                    )
                    bb_calls += 1
                    bb_net.add(self.bb_home)
                # logger.info(f"process fashion bounds: {time.time()-start} sec")
                # add uniq bounds
                for item in uniq_bounds:
                    if item in unique_bounds_count:
                        unique_bounds_count[item] += 1
                    else:
                        unique_bounds_count[item] = 1
                # logger.info(f"process uniq: {time.time()-start} sec")
            except Exception as error:
                logger.exception(error)
                raise
        return unique_bounds_count, bb_calls, list(bb_net)

    @staticmethod
    def set_bounds_obj(image_json, optional_bbs) -> Tuple[str, list]:
        image_to_index = image_json["image_url"]
        bounds = [
            cur_bound
            for cur_bound in image_json["bb_rec"]
            if cur_bound["label"] in optional_bbs
        ]
        return image_to_index, bounds

    async def find_deeptags(
        self,
        requirements: Requirements,
        image_dict: dict,
        optional_bbs: list,
        main_bb: str,
        cbir_net: str,
    ) -> tuple:
        cbir_calls = 0
        raw_results = []
        max_bb_prob = 0
        image_to_index = ""
        bounds: list = []
        cbir_client: InferClient = requirements.clients[cbir_net]
        for image_url, img_value in image_dict.items():
            try:
                img_res = []
                image_json = img_value["image_json"]
                for cur_bound in image_json["bb_rec"]:
                    if not cur_bound["label"] in optional_bbs:
                        img_res.append(
                            {
                                "bb_results": cur_bound,
                            }
                        )
                        continue
                    cbir_prediction, _details = await cbir_client.predict(
                        Input(
                            entities=[
                                InputEntity(
                                    data=img_value["image_bin"],
                                    options={"bounds": [cur_bound[consts.BOX]]},
                                )
                            ]
                        )
                    )
                    cbir_results = cbir_prediction.entities[0].mapping
                    cbir_calls += 1
                    category = cbir_results["category"].data.decode("utf-8")
                    raw_deeptags: dict = orjson.loads(cbir_results["deeptags"].data)
                    cur_bound["network"] = self.cbir_network_selector[
                        cur_bound["label"]
                    ]
                    cur_bound["cbir_name"] = cbir_net
                    img_res.append(
                        {
                            "cbir_category": category,
                            "raw_cbir": raw_deeptags,
                            "deep_tags": self.filter_cbir_tags(raw_deeptags, main_bb),
                            "bb_results": cur_bound,
                            "requested_BB": main_bb,
                        }
                    )
                    if cur_bound["probability"] > max_bb_prob:
                        max_bb_prob = cur_bound["probability"]
                        signature_vector: bytes = cbir_results["vector"].data
                        if image_url != image_to_index:
                            image_to_index, bounds = self.set_bounds_obj(
                                image_json, optional_bbs
                            )
                if len(img_res) == 0:
                    img_res = [image_json]
                raw_results.append(img_res)
            except Exception as error:
                logger.exception(error)
                raise
        # check if needed
        if max_bb_prob == 0:
            for img_value in image_dict.values():
                cbir_prediction, _details = await cbir_client.predict(
                    Input(
                        entities=[
                            InputEntity(
                                data=img_value["image_bin"],
                                options={"bounds": [[0.1, 0.1, 0.9, 0.9]]},
                            )
                        ]
                    )
                )
                cbir_results = cbir_prediction.entities[0].mapping
                cbir_calls += 1
                break
            signature_vector = cbir_results["vector"].data
        return (
            raw_results,
            signature_vector,
            bounds,
            image_to_index,
            cbir_calls,
        )

    async def get_tagit_results(
        self, requirements: Requirements, item_data: dict
    ) -> dict:
        tagit_client: InferClient = requirements.clients[self.tagit_network]
        data_to_model = {
            "products": [
                {
                    "categories": item_data["categories"],
                    "sku": item_data["sku"],
                    "title": item_data["title"],
                }
            ]
        }
        # Model classification
        input = Input(
            entities=[InputEntity(data=json.dumps(data_to_model).encode("utf8"))]
        )
        tagit_res, details = await tagit_client.predict(input)
        return json.loads(tagit_res.entities[0].data)

    @staticmethod
    def find_max_bound(bound_bank: list, unique_bounds_count: dict) -> str:
        max_bound = ""
        max_count = 0
        for bound in bound_bank:
            if bound in unique_bounds_count and unique_bounds_count[bound] > max_count:
                max_bound = bound
                max_count = unique_bounds_count[bound]
        return max_bound

    def find_max_unique_bound(self, bound_bank: list, unique_bounds_count: dict) -> str:
        max_bound = ""
        max_count = 0
        for bound in bound_bank:
            for sim_bound in self.similarbbs[bound]:
                if (
                    sim_bound in unique_bounds_count
                    and unique_bounds_count[sim_bound] > max_count
                ):
                    max_bound = bound
                    max_count = unique_bounds_count[sim_bound]
        return max_bound

    def build_bound_bank(self, item_data) -> list:
        bound_bank = set("")
        # build bound bank
        for label in item_data[consts.TAGS]:
            # tag can be fallback add only tags with BB mapping
            if label in self.label2bb:
                for bound in self.label2bb[label]:
                    bound_bank.add(bound)
        return list(bound_bank)

    def select_catalog(self, bound_bank: list) -> list:
        # check bound bank catalogs
        catalogs = set("")
        for bound in bound_bank:
            if bound in self.fashion_cat:
                catalogs.add(consts.FASHION)
            elif bound in self.home_cat:
                catalogs.add(consts.HOME)
        return list(catalogs)

    async def select_bb_for_item(
        self,
        requirements: Requirements,
        item_data,
        unique_bounds_count: dict,
        bound_bank: list,
    ) -> tuple:

        # we may have more than one bound
        # we select the bound from the majority of the BBs
        max_tag_bound = self.find_max_bound(bound_bank, unique_bounds_count)
        if max_tag_bound != "":
            return self.set_bb_return_obj(
                max_tag_bound, "Use item tagging", bound_bank, unique_bounds_count
            )

        # case where we have categories but none of them were found. we select the last one (random selection)
        if len(bound_bank) > 0:
            for bound in bound_bank:
                random_bound = bound
                break
            # check if we have fallback in
            if len(unique_bounds_count) == 0:
                return self.set_bb_return_obj(
                    random_bound,
                    "Use item tag with random bound, No bounds found",
                    bound_bank,
                    unique_bounds_count,
                )
            max_unique_bound = self.find_max_unique_bound(
                bound_bank, unique_bounds_count
            )
            if max_unique_bound != "":
                return self.set_bb_return_obj(
                    max_unique_bound,
                    "Use item tagging, select similar BB category",
                    bound_bank,
                    unique_bounds_count,
                )
            # we don't have max bound select random one
            return self.set_bb_return_obj(
                random_bound,
                "Use item tagging with random bound, No bounds matching tagging",
                bound_bank,
                unique_bounds_count,
            )
        if len(unique_bounds_count) == 0:
            return self.set_bb_return_obj(
                "",
                "Unique bound count is empty,  No bounds found",
                bound_bank,
                unique_bounds_count,
            )
        item_tag = flows.set_main_bb(unique_bounds_count)
        # if we found valid Bounding Box
        if item_tag in self.label2similars:
            return self.set_bb_return_obj(
                item_tag, "Use max unique bound", bound_bank, unique_bounds_count
            )

        return await self.tagit_selections(
            requirements, item_data, unique_bounds_count, bound_bank
        )

    async def tagit_selections(
        self, requirements, item_data, unique_bounds_count, bound_bank
    ):
        tagit_results = await self.get_tagit_results(requirements, item_data)
        if tagit_results[tagit_consts.OUTPUT_I2BB_FIELD] in unique_bounds_count:
            return self.set_bb_return_obj(
                tagit_results[tagit_consts.OUTPUT_I2BB_FIELD],
                "Use I2BB bound",
                bound_bank,
                unique_bounds_count,
                tagit_results,
            )
        if tagit_results[tagit_consts.OUTPUT_TITLE_TO_CAT_FIELD] in unique_bounds_count:
            return self.set_bb_return_obj(
                tagit_results[tagit_consts.OUTPUT_TITLE_TO_CAT_FIELD],
                "Use title2category bound",
                bound_bank,
                unique_bounds_count,
                tagit_results,
            )
        # check similar bounds
        for sim_bound in self.similarbbs[tagit_results[tagit_consts.OUTPUT_I2BB_FIELD]]:
            if sim_bound in unique_bounds_count:
                return self.set_bb_return_obj(
                    tagit_results[tagit_consts.OUTPUT_I2BB_FIELD],
                    "Use I2BB bound, select similar BB category",
                    bound_bank,
                    unique_bounds_count,
                    tagit_results,
                )
        for sim_bound in self.similarbbs[
            tagit_results[tagit_consts.OUTPUT_TITLE_TO_CAT_FIELD]
        ]:
            if sim_bound in unique_bounds_count:
                return self.set_bb_return_obj(
                    tagit_results[tagit_consts.OUTPUT_TITLE_TO_CAT_FIELD],
                    "Use title2category bound, select similar BB category",
                    bound_bank,
                    unique_bounds_count,
                    tagit_results,
                )

        return self.set_bb_return_obj(
            "",
            "Couldn't find category using flow, No deeptags",
            bound_bank,
            unique_bounds_count,
            tagit_results,
        )

    def set_new_cat(self, selected_bound: str, cur_tags: list) -> list:
        if selected_bound == "":
            return []
        for label in cur_tags:
            # tag can be fallback add only tags with BB mapping
            if label in self.label2bb and selected_bound in self.label2bb[label]:
                return []

        # we don't a match but we have new category.
        return [self.bb2label[selected_bound]]

    def get_catalog(self, process_params: dict, bound_bank: list) -> list:
        if "force_catalog" in process_params:
            use_catalog = process_params["force_catalog"]
        else:
            use_catalog = self.select_catalog(bound_bank)
        return use_catalog

    async def calculate_default_bound(
        self, main_bb, requirements, image_dict, feed_analysis
    ) -> Tuple[dict, bytes]:
        if main_bb != "":
            logger.error("main_bb %r not in CBIR_NETWORK_SELECTOR", main_bb)

        cbir_client: InferClient = requirements.clients[self.cbir_networks["fashion"]]
        for img_value in image_dict.values():
            cbir_prediction, _details = await cbir_client.predict(
                Input(
                    entities=[
                        InputEntity(
                            data=img_value["image_bin"],
                            options={"bounds": [[0.1, 0.1, 0.9, 0.9]]},
                        )
                    ]
                )
            )
            cbir_results = cbir_prediction.entities[0].mapping
            feed_analysis["Cbir Calls"] = 1
            break
        signature_vector = cbir_results["vector"].data
        feed_analysis["CBIR network"] = self.cbir_networks["fashion"]
        image_json = []
        for image in image_dict.values():
            image_json.append([image["image_json"]])
        ret_dict = flows.set_return_obj(
            ctl_image={},
            tagging_json=[],
            tagging={},
            lexicon_tagging={},
            debug_vmr=image_json,
            bounds=[],
            image_to_index="",
            bb_categories=[],
            feed_analysis=feed_analysis,
            tags=[],
        )
        return ret_dict, signature_vector

    async def process(
        self,
        requirements: Requirements,
        image_source: Dict[str, bytes],
        item_data: dict,
        process_params: dict = None,
    ) -> Tuple[dict, bytes]:
        result: Prediction = await self.infer(
            requirements=requirements,
            input=Input(
                entities=[
                    InputEntity(
                        mapping={
                            image_url: InputEntity(data=image)
                            for image_url, image in image_source.items()
                        },
                        options=item_data,
                    )
                ]
            ),
            process_params=(process_params or {}),
        )
        return result.entities[0].details, result.entities[0].data

    async def infer(
        self,
        requirements: Requirements,
        input: Input,
        process_params: dict = {},
    ) -> Prediction:
        """

        :param requirements: requirements to have all IO for this flow
        :param image_source:
        :param item_data: All the data, awailable for the parameter. Contains:
            - sku - item sku
            - tags - item tags

        """
        image_source = {
            key: entity.data for key, entity in input.entities[0].mapping.items()
        }
        item_data = input.entities[0].options

        start = time.time()
        logger.info("started SKU %r", item_data["sku"])
        image_dict, bad_image_urls = flows.preprocess_inputs(image_source, yolo=True)
        logger.debug("Checked Inputs Duration:  %r sec", time.time() - start)
        feed_analysis = {
            consts.SELECTED_BOUND: "",
            consts.BOUNDS_BANK: [],
            consts.BB_SELECTION_FLOW: "All Images Urls are invalid",
            "bad_images_urls": bad_image_urls,
            consts.UNIQUE_BOUNDS_COUNT: {},
            "deep_tags_len": 0,
            "bad_images_num": len(bad_image_urls),
            "images_num": len(image_dict),
            "CTL bounds num": 0,
            "CBIR network": "None",
            "BB networks": [],
            "ClusterChange": False,
            "OriginalCluster": item_data["tags"],
            "Cbir Calls": 0,
            "BB Calls": 0,
            "thematic_att_num": 0,
            "thematic_values_add": 0,
            "thematic_trans": 0,
            "thematic_total_tags": 0,
        }
        if len(image_dict) == 0:
            image_json = []
            for image_dict in image_dict.values():
                image_json.append([image_dict.get("image_json", {})])
            feed_analysis["duration"] = time.time() - start
            prediction = flows.set_signature_vector(np.array([]), np.array([]))
            prediction.details = json.dumps(
                flows.set_return_obj(
                    ctl_image={},
                    tagging_json=[],
                    tagging={},
                    lexicon_tagging={},
                    debug_vmr=image_json,
                    bounds=[],
                    image_to_index="",
                    bb_categories=[],
                    feed_analysis=feed_analysis,
                    tags=[],
                )
            ).encode("utf8")
            return Prediction(entities=[prediction])
        # we have urls => extract all BBs
        bound_bank = self.build_bound_bank(item_data)
        use_catalog = self.get_catalog(process_params, bound_bank)
        unique_bounds_count, bb_calls, bb_net = await self.get_bounds(
            requirements, image_dict, item_data, use_catalog
        )
        feed_analysis["BB Calls"] = bb_calls
        feed_analysis["BB networks"] = bb_net
        logger.info("Get bounds  %r sec", time.time() - start)
        # select bound
        optional_bbs, bb_analysis, main_bb = await self.select_bb_for_item(
            requirements, item_data, unique_bounds_count, bound_bank
        )
        logger.info("Select BB:  %r sec", time.time() - start)
        feed_analysis.update(bb_analysis)
        # no bound for item, calculate Signature vector and exit
        if main_bb not in self.cbir_network_selector:
            ret_dict, signature_vector = await self.calculate_default_bound(
                main_bb, requirements, image_dict, feed_analysis
            )
            end = time.time()
            feed_analysis["duration"] = end - start
            logger.info("process ended duration (no CBIR):  %r sec", end - start)

            return Prediction(
                entities=[
                    PredictionEntity(
                        data=signature_vector,
                        details=ret_dict,
                    )
                ]
            )
        try:
            cbir_net = self.cbir_networks[self.cbir_network_selector[main_bb]]
        except BaseException as ex:
            logger.error(ex)
            raise TypeError(
                f"got Network type: {self.cbir_network_selector[main_bb]}, the code support only 'fashion', 'jewelry', 'home"
            ) from ex

        (
            raw_results,
            signature_vector,
            bounds,
            image_to_index,
            cbir_calls,
        ) = await self.find_deeptags(
            requirements, image_dict, optional_bbs, main_bb, cbir_net
        )
        logger.info("Deep tags:  %r sec", time.time() - start)
        feed_analysis["Cbir Calls"] = cbir_calls
        deep_tags_prob = flows.set_tagging_json(raw_results)
        logger.info("Tagging Json:  %r sec", time.time() - start)
        tagging = flows.set_tagging(deep_tags_prob)
        tagging["Cat"] = [main_bb]
        logger.info("Tagging:  %r sec", time.time() - start)
        if "Lexicon" in process_params:
            lexicon_name = process_params["Lexicon"]
        else:
            lexicon_name = "syte_rba"

        logger.info("use lexicon %r", lexicon_name)
        lexicon_tagging, thematic_num, lexicon_dt = self.lexicons.process(
            None,
            None,
            {"tagging": tagging, "bbCategory": main_bb},
            {"lexicon_name": lexicon_name},
        )
        feed_analysis["thematic_att_num"] = len(lexicon_tagging)
        feed_analysis["thematic_values_add"] = thematic_num
        feed_analysis["thematic_trans"] = lexicon_dt
        feed_analysis["thematic_total_tags"] = lexicon_dt + thematic_num

        ctl_image, ctl_stats = flows.set_ctl(
            self.cbir_network_selector[main_bb], optional_bbs, image_dict
        )
        logger.info("CTL:  %r sec", time.time() - start)
        feed_analysis.update(ctl_stats)
        feed_analysis["deep_tags_len"] = len(tagging)
        feed_analysis["duration"] = time.time() - start
        feed_analysis["CBIR network"] = cbir_net

        tags = self.set_new_cat(feed_analysis["Selected bound"], item_data[consts.TAGS])
        if len(tags) > 0:
            feed_analysis["ClusterChange"] = True
            # fix all tags fields cross the board.
            fix_tags(bounds, ctl_image, tags)

        ret_dict = flows.set_return_obj(
            ctl_image=ctl_image,
            tagging_json=list(deep_tags_prob.values()),
            tagging=tagging,
            lexicon_tagging=lexicon_tagging,
            debug_vmr=raw_results,
            bounds=bounds,
            image_to_index=image_to_index,
            bb_categories=[main_bb],
            feed_analysis=feed_analysis,
            tags=tags,
        )
        end = time.time()
        logger.info("process ended duration:  %r sec", end - start)

        return Prediction(
            entities=[
                PredictionEntity(
                    data=signature_vector, details=ret_dict
                )
            ]
        )
