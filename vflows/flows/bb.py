import logging
import json
import asyncio
from pathlib import Path
from typing import Dict, Any

from vflows.flows.base_flow import BaseFlow
from vflows.requirements import Requirements
from sytedt.client import InferClient
from vflows.utils import consts, flows

from sytedt.inferer import Input, InputEntity, Prediction, PredictionEntity

logger = logging.getLogger(__name__)


class BB(BaseFlow):
    def __init__(self, base_path: Path, config: Dict[str, Any]):
        super().__init__(base_path, config)
        self.bb_fashion = next(iter(config["yolo_fashion_sid"]))
        self.bb_person = next(iter(config["yolo_person_sid"]))
        self.bb_home = next(iter(config["yolo_home_sid"]))
        with open(base_path / config["label2bb"], "rt", encoding="utf-8") as f:
            label2bb = json.load(f)
        self.bb2label = {}
        for label, bb_list in label2bb.items():
            for bb in bb_list:
                if bb not in self.bb2label:
                    self.bb2label[bb] = []
                self.bb2label[bb].append(label)

    def get_requirements(self):
        return [self.bb_fashion, self.bb_home, self.bb_person]

    def set_bounds(
        self,
        bounds: list,
        catalog: str,
        gender: str,
    ) -> None:
        for cur_bound in bounds:
            dict_obj = cur_bound
            dict_obj["catalog"] = catalog
            dict_obj["tags"] = self.bb2label[dict_obj["labels"]]
            # REMOVE ME after matan fix the code.
            dict_obj["label"] = dict_obj["labels"]
            dict_obj["gender"] = gender
            dict_obj["x0"] = dict_obj["box"][0]
            dict_obj["y0"] = dict_obj["box"][1]
            dict_obj["x1"] = dict_obj["box"][2]
            dict_obj["y1"] = dict_obj["box"][3]
            dict_obj["scaled_x0"] = dict_obj["box"][0]
            dict_obj["scaled_y0"] = dict_obj["box"][1]
            dict_obj["scaled_x1"] = dict_obj["box"][2]
            dict_obj["scaled_y1"] = dict_obj["box"][3]

    async def process(
        self,
        requirements: Requirements,
        image_source: dict,
        item_data: dict,
        process_params: dict,
    ) -> Any:
        raise NotImplementedError

    async def infer(
        self,
        requirements: Requirements,
        input: Input,
        process_params: dict = {},
    ) -> Prediction:
        image_source = input.entities[0].data
        item_data = json.loads(input.entities[1].data)

        if item_data is None:
            item_data = {"catalogs": [consts.FASHION, consts.HOME]}
        bb_image_bin = flows.resize_image(image_source)
        bb_input = Input(entities=[InputEntity(data=bb_image_bin)])
        if consts.FASHION in item_data["catalogs"]:
            bb_fashion: InferClient = requirements.clients[self.bb_fashion]
            bb_person: InferClient = requirements.clients[self.bb_person]
            bb_predictions = await asyncio.gather(
                *[bb_fashion.predict(bb_input), bb_person.predict(bb_input)]
            )
            bb_results = [
                self._prediction_to_bounds(prediction) for prediction in bb_predictions
            ]
            gender = flows.select_gender(bb_results[1])
            self.set_bounds(bb_results[0], consts.FASHION, gender)
            fashion_bounds = bb_results[0]
        else:
            fashion_bounds = []
        if consts.HOME in item_data["catalogs"]:
            bb_home: InferClient = requirements.clients[self.bb_home]
            home_prediction = await bb_home.predict(bb_input)
            home_bounds = self._prediction_to_bounds(home_prediction)
            gender = consts.HD
            self.set_bounds(home_bounds, consts.HD, gender)
        else:
            home_bounds = []

        bounds = [*fashion_bounds, *home_bounds]

        return Prediction(
            entities=[PredictionEntity(data=json.dumps(bounds).encode("utf8"))]
        )

    @staticmethod
    def _prediction_to_bounds(prediction):
        result = []
        for entity in prediction.entities:
            result.extend([json.loads(e.data) for e in entity.listing])
        return result
