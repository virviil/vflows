import logging
from typing import Any, Dict, List, Tuple
import cv2
import numpy as np
from vflows.utils import consts

MIN_IMAGE_LEN = 700
MAX_IMAGE_LEN = 500000
IMAGE_DIM = 608
MAX_IMAGE_DIM = 1024


logger = logging.getLogger(__name__)


def resize_image(image: bytes, image_dim: int = IMAGE_DIM, quality: int = 100) -> bytes:
    nparr = np.fromstring(image, np.uint8)

    img = cv2.imdecode(nparr, cv2.IMREAD_UNCHANGED)
    (height, width) = img.shape[:2]
    factor = image_dim / float(height)
    dim = (int(width * factor), image_dim)

    # resize the image
    image_bin2 = cv2.resize(img, dim, interpolation=cv2.INTER_AREA)
    return cv2.imencode(".jpg", image_bin2, [int(cv2.IMWRITE_JPEG_QUALITY), quality])[
        1
    ].tobytes()


def preprocess_inputs(
    image_source: Dict[str, bytes],
    max_image_len: int = MAX_IMAGE_LEN,
    yolo: bool = False,
    image_qual: int = 90,
) -> Tuple[Dict[str, Any], List[str]]:
    """
    Function parses the inputs data for the flow. It checks if the data has come in dictionary form, and the
    it check sizes of the images. If the image is too big, it resizes it to YOLO input tensor size and saves it to the memory.
    """
    bad_urls = []
    image_dict = {}
    for image_url, image in image_source.items():
        image_size = len(image)
        if image_size < MIN_IMAGE_LEN:
            bad_urls.append(image_url)
            continue
        if yolo:
            yolo_image = resize_image(image)
        else:
            yolo_image = bytes()
        if image_size > max_image_len:
            image = resize_image(image, MAX_IMAGE_DIM, image_qual)
        image_dict[image_url] = {
            "image_json": {
                "image_url": image_url,
                "image_size": image_size,
                "bb_size": len(yolo_image),
            },
            "image_bin": image,
            "image_bb": yolo_image,
        }
    return image_dict, bad_urls


def set_return_obj(
    ctl_image: dict,
    tagging_json: list,
    tagging: dict,
    lexicon_tagging: dict,
    debug_vmr: list,
    bounds: list,
    image_to_index: str,
    bb_categories: list,
    feed_analysis: dict,
    tags: list,
) -> dict:
    ret_obj = {
        "ctl_image": ctl_image,
        "tagging_json": tagging_json,
        "tagging": tagging,
        "lexicon_tagging": lexicon_tagging,
        "debug_vmr": debug_vmr,
        "bounds": bounds,
        "bbCategories": bb_categories,
        consts.FEED_ANALYSIS: feed_analysis,
    }
    if image_to_index != "":
        ret_obj["imageToIndex"] = image_to_index
    if tags != []:
        ret_obj["tags"] = tags
    return ret_obj


def set_tagging(deep_tags_prob: dict) -> Dict[str, List[str]]:
    ret_list = {}
    for deeptag in deep_tags_prob.values():
        if deeptag["name"] not in ret_list:
            ret_list[deeptag["name"]] = [deeptag["value"]]
        else:
            logger.warning("two values for one attribute: %r", deep_tags_prob)
            ret_list[deeptag["name"]].append(deeptag["value"])
    return ret_list


def set_tagging_json(raw_results: dict) -> dict:
    unified_deeptags = {}
    for raw_result in raw_results:
        for cbir in raw_result:
            if "deep_tags" not in cbir:
                continue
            for deep_tag in cbir["deep_tags"]:
                if deep_tag["name"] not in unified_deeptags:
                    unified_deeptags[deep_tag["name"]] = deep_tag
                else:
                    if (
                        unified_deeptags[deep_tag["name"]]["probability"]
                        < deep_tag["probability"]
                    ):
                        unified_deeptags[deep_tag["name"]] = deep_tag
    return unified_deeptags


def set_main_bb(unique_bounds_count: dict) -> str:
    # select the unique max BB or None
    if len(unique_bounds_count) > 0:
        val = list(unique_bounds_count.values())
        key = list(unique_bounds_count.keys())
        max_value = max(val)
        if val.count(max_value) == 1:
            return key[val.index(max(val))]
    return ""


def set_ctl(catalog, optional_bbs, image_dict) -> tuple:
    max_non_bb_bounds = 0
    for image_url, img_value in image_dict.items():
        image_json = img_value["image_json"]
        non_bb_bounds = 0
        for cur_bound in image_json["bb_rec"]:
            if (
                cur_bound["catalog"] == catalog
                and cur_bound["label"] not in optional_bbs
            ):
                non_bb_bounds += 1
        if non_bb_bounds > max_non_bb_bounds:
            max_non_bb_bounds = non_bb_bounds
            max_ctl_obj = image_json["bb_rec"]
            max_image_url = image_url
    if max_non_bb_bounds > 0:
        ctl_obj = {"url": max_image_url, "bounds": []}
        for cur_bound in max_ctl_obj:
            if (
                cur_bound["catalog"] == catalog
                and cur_bound["label"] not in optional_bbs
            ):
                ctl_obj["bounds"].append(cur_bound)
        ctl_stats = {"CTL bounds num": max_non_bb_bounds}
        return ctl_obj, ctl_stats
    return {}, {"CTL bounds num": 0}


def select_gender(bounds: dict) -> str:
    # simple algorithm for selecting gender:
    # 1. head - 21
    # 2. full body - 10
    # 3. lower body - 4
    gender = {
        "baby": 0,
        "boy": 0,
        "girl": 0,
        "female": 0,
        "male": 0,
        # remove me
        "Boy": 0,
        "Baby": 0,
        "Girl": 0,
        "Woman": 0,
        "Man": 0,
    }
    # FullPerson-Baby
    # FullPerson-Boy
    # FullPerson-Girl
    # FullPerson-Man
    # FullPerson-Woman
    # Head-Baby
    # Head-Boy
    # Head-Girl
    # Head-Man
    # Head-Woman
    # LowerBody-Baby
    # LowerBody-Boy
    # LowerBody-Girl
    # LowerBody-Man
    # LowerBody-Woman
    if len(bounds) == 0:
        return ""
    for bound in bounds:
        if "FullPerson" in bound["labels"]:
            gender[bound["labels"].split("-")[1]] += 10
        if "Head" in bound["labels"]:
            gender[bound["labels"].split("-")[1]] += 21
        if "LowerBody" in bound["labels"]:
            gender[bound["labels"].split("-")[1]] += 4

    val = list(gender.values())
    key = list(gender.keys())
    return key[val.index(max(val))]
