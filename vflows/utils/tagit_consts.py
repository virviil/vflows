# Textual models
ITEM_LIST_FIELD = "products"
ITEM_ID_FIELD = "sku"
TITLE_FIELD = "title"
CATEGORIES_FIELD = "categories"

MODEL_FEATURES = [ITEM_ID_FIELD, TITLE_FIELD, CATEGORIES_FIELD]
MODEL_OUTPUT_FIELD = "results"
MODEL_LABELS_FIELD = "labels"

OUTPUT_PREFIX = "field"
OUTPUT_VERTICAL_GENDER_FIELD_VERTICAL = "vertical"
OUTPUT_VERTICAL_GENDER_FIELD_GENDER = "gender"
OUTPUT_VERTICAL_GENDER_FIELD_AGE_ATTRIBUTE = "age_attribute"
OUTPUT_VERTICAL_GENDER_FIELD_SIZE_ATTRIBUTE = "size_attribute"
OUTPUT_VERTICAL_GENDER_FIELD_GENDER_ATTRIBUTE = "sex_attribute"
OUTPUT_VERTICAL_GENDER_LIST = [
    OUTPUT_VERTICAL_GENDER_FIELD_VERTICAL,
    OUTPUT_VERTICAL_GENDER_FIELD_AGE_ATTRIBUTE,
    OUTPUT_VERTICAL_GENDER_FIELD_SIZE_ATTRIBUTE,
    OUTPUT_VERTICAL_GENDER_FIELD_GENDER_ATTRIBUTE,
]
OUTPUT_INDEXED_CAT_FIELD = "categories"
OUTPUT_TITLE_TO_CAT_FIELD = "implementation_categories"
OUTPUT_TITLE_TO_CAT_LIST = [OUTPUT_TITLE_TO_CAT_FIELD]
OUTPUT_I2BB_FIELD = "bb_categories"
OUTPUT_I2BB_FIELD_MAPPED = f"{OUTPUT_I2BB_FIELD}_implementation_mapping"
OUTPUT_I2BB_LIST = [OUTPUT_I2BB_FIELD]
OUTPUT_I2BB_FASHION_FIELD = "bb_categories_fashion"
OUTPUT_I2BB_FASHION_LIST = [OUTPUT_I2BB_FASHION_FIELD]
OUTPUT_I2BB_FASHION_FIELD_MAPPED = f"{OUTPUT_I2BB_FASHION_FIELD}_implementation_mapping"
OUTPUT_I2BB_HD_FIELD = "bb_categories_hd"
OUTPUT_I2BB_HD_LIST = [OUTPUT_I2BB_HD_FIELD]
OUTPUT_I2BB_HD_FIELD_MAPPED = f"{OUTPUT_I2BB_HD_FIELD}_implementation_mapping"
UNMATCHED_LABEL = "Unmatched"
# Model names
MODEL_NAME_TITLE_TO_CAT = "titletocat"
MODEL_NAME_VERTICAL_GENDER = "verticalgender"
MODEL_NAME_TAGIT = "tagit"
MODEL_NAME_EFFICIENTNET = "efficientnet"
MODEL_NAME_TFCBIR = "tfcbir"
MODEL_NAME_I2BB = "i2bb"

# Model names - alias
MODEL_NAME_TITLE_TO_CAT_ALIAS_1 = "title2cat"
MODEL_NAME_TITLE_TO_CAT_ALIAS_2 = "title2category"
MODEL_NAME_TITLE_TO_CAT_ALIAS_3 = "titletocategory"
MODEL_NAME_VERTICAL_GENDER_ALIAS_1 = "verticalgenderclassifier"
MODEL_NAME_VERTICAL_GENDER_ALIAS_2 = "vertical_genderclassifier"
MODEL_NAME_VERTICAL_GENDER_ALIAS_3 = "vertical_gender"
MODEL_NAME_TAGIT_ALIAS_1 = "tagger"
MODEL_NAME_TAGIT_ALIAS_2 = "auto_tagger"
MODEL_NAME_TAGIT_ALIAS_3 = "auto_tagit"
MODEL_NAME_TAGIT_ALIAS_4 = "taggit"
MODEL_NAME_EFFICIENTNET_ALIAS_1 = "cbir_efficientnet"
MODEL_NAME_EFFICIENTNET_ALIAS_2 = "tfcbir_efficientnet"
MODEL_NAME_EFFICIENTNET_ALIAS_3 = "efficient_net"
MODEL_NAME_TFCBIR_ALIAS_1 = "vgg16"
MODEL_NAME_TFCBIR_ALIAS_2 = "cbir_vgg16"
MODEL_NAME_TFCBIR_ALIAS_3 = "tfcbir_vgg16"
MODEL_NAME_I2BB_ALIAS_1 = "item2bb"
MODEL_NAME_I2BB_ALIAS_2 = "itobb"
MODEL_NAME_I2BB_ALIAS_3 = "itemtobb"

MODEL_NAME_TITLE_TO_CAT_LIST = [
    MODEL_NAME_TITLE_TO_CAT,
    MODEL_NAME_TITLE_TO_CAT_ALIAS_1,
    MODEL_NAME_TITLE_TO_CAT_ALIAS_2,
    MODEL_NAME_TITLE_TO_CAT_ALIAS_3,
]

MODEL_NAME_VERTICAL_GENDER_LIST = [
    MODEL_NAME_VERTICAL_GENDER,
    MODEL_NAME_VERTICAL_GENDER_ALIAS_1,
    MODEL_NAME_VERTICAL_GENDER_ALIAS_2,
    MODEL_NAME_VERTICAL_GENDER_ALIAS_3,
]

MODEL_NAME_TAGIT_LIST = [
    MODEL_NAME_TAGIT,
    MODEL_NAME_TAGIT_ALIAS_1,
    MODEL_NAME_TAGIT_ALIAS_2,
    MODEL_NAME_TAGIT_ALIAS_3,
    MODEL_NAME_TAGIT_ALIAS_4,
]

MODEL_NAME_EFFICIENTNET_LIST = [
    MODEL_NAME_EFFICIENTNET,
    MODEL_NAME_EFFICIENTNET_ALIAS_1,
    MODEL_NAME_EFFICIENTNET_ALIAS_2,
    MODEL_NAME_EFFICIENTNET_ALIAS_3,
]

MODEL_NAME_TFCBIR_LIST = [
    MODEL_NAME_TFCBIR,
    MODEL_NAME_TFCBIR_ALIAS_1,
    MODEL_NAME_TFCBIR_ALIAS_2,
    MODEL_NAME_TFCBIR_ALIAS_3,
]

MODEL_NAME_I2BB_LIST = [
    MODEL_NAME_I2BB,
    MODEL_NAME_I2BB_ALIAS_1,
    MODEL_NAME_I2BB_ALIAS_2,
    MODEL_NAME_I2BB_ALIAS_3,
]

SUPPORTED_MODELS = [
    MODEL_NAME_TITLE_TO_CAT,
    MODEL_NAME_VERTICAL_GENDER,
    MODEL_NAME_TAGIT,
    MODEL_NAME_EFFICIENTNET,
    MODEL_NAME_TFCBIR,
    MODEL_NAME_I2BB,
]

# Map I2BB to the implementation categories terminology
IS_MAP_I2BB = True
MAP_I2BB_LABELS_TO_CLUSTERS = {
    "AirConditioning": "AirConditioning",
    "ArmoiresWardrobes": "ArmoiresWardrobes",
    "Art": "Art",
    "Backpacks": "Backpacks",
    "Bags": "Allbags",
    "BathroomFittings": "BathroomFittings",
    "Bedding": "Bedding",
    "Beds": "Beds",
    "Belts": "Belts",
    "Benches": "Benches",
    "Blankets": "Blankets",
    "Blender": "Blender",
    "Bookends": "Bookends",
    "Boots": "Boots",
    "Bottles": "Bottles",
    "Bowls": "Bowls",
    "Bracelets": "Bracelets",
    "BreadMaker": "BreadMaker",
    "Cabinets": "Cabinets",
    "CandlesCandleholders": "CandlesCandleholders",
    "CapsulesCoffeeMachine": "CoffeeAppliances",
    "Chaises": "Chaises",
    "ChemexCoffeeMaker": "CoffeeAppliances",
    "Clocks": "Clocks",
    "Coatracks": "Coatracks",
    "Coats": "CoatsJacketsSuits",
    "CoffeeGrinder": "CoffeeAppliances",
    "CookingUtensils": "CookingUtensils",
    "Cookware": "Cookware",
    "CoverUps": "CoverUps",
    "Cufflinks": "Cufflinks",
    "Curtains": "Curtains",
    "Decor": "Decor",
    "Dinnerware": "Dinnerware",
    "Dishwashers": "KitchenAppliances",
    "Dresses": "Dresses",
    "DripCoffeeMaker": "CoffeeAppliances",
    "Earrings": "Earrings",
    "EspressoMachines": "CoffeeAppliances",
    "Eyeglasses": "Eyewear",
    "Fans": "Fans",
    "FoodProcessors": "KitchenAppliances",
    "Footstools": "Footstools",
    "FrenchPresses": "CoffeeAppliances",
    "Fryers": "KitchenAppliances",
    "GlovesAndMitten": "GlovesAndMitten",
    "HandMixer": "KitchenAppliances",
    "Hats": "Hats",
    "HeadPhones": "HeadPhones",
    "HomeAppliances": "HomeAppliances",
    "IslandsAndCarts": "IslandsAndCarts",
    "Jackets": "CoatsJacketsSuits",
    "Juicers": "KitchenAppliances",
    "Jumpsuits": "Jumpsuits",
    "Kettles": "KitchenAppliances",
    "KeyChains": "KeyChains",
    "Kimonos": "Kimonos",
    "KitchenAccessories": "KitchenAccessories",
    "KitchenAppliances": "KitchenAppliances",
    "KitchenVent": "Vents",
    "Ladders": "Ladders",
    "LaundryHampers": "Storage",
    "Lighting": "Lighting",
    "Makeup": "Makeup",
    "Microwaves": "KitchenAppliances",
    "Mirrors": "Mirrors",
    "MokaPot": "CoffeeAppliances",
    "Necklaces": "Necklaces",
    "NightMorning": "NightMorning",
    "Nightwear": "NightMorning",
    "Ovens": "KitchenAppliances",
    "Perfumes": "Perfumes",
    "PhoneCovers": "PhoneCovers",
    "Phones": "Phones",
    "Pillows": "Pillows",
    "Planters": "Planters",
    "PouchBags": "Allbags",
    "Poufs": "Poufs",
    "Ranges": "HomeAppliances",
    "Refrigerators": "Refrigerators",
    "RiceCookers": "KitchenAppliances",
    "Rings": "Rings",
    "RobotVacuumCleaner": "VacuumCleaners",
    "RoomDividers": "RoomDividers",
    "RugsCarpets": "RugsCarpets",
    "Sandals": "Sandals",
    "SanitaireWare": "SanitaireWare",
    "Scarves": "Scarves",
    "Seats": "Seats",
    "Serveware": "Serveware",
    "Shelves": "Shelves",
    "Shirts": "PulloverAndShirts",
    "Shoes": "AllShoes",
    "Shorts": "Shorts",
    "ShowerCabins": "ShowerCabins",
    "ShowerHeads": "ShowerHeads",
    "Sinks": "Sinks",
    "Skirts": "Skirts",
    "SocksAndTights": "SocksAndTights",
    "Sofas": "Sofas",
    "SportShoes": "AllShoes",
    "StandMixer": "KitchenAppliances",
    "StickBlender": "StickBlender",
    "StorageBasketsBoxesContainers": "StorageOrganization",
    "StorageOrganization": "StorageOrganization",
    "Stoves": "KitchenAppliances",
    "Suitcases": "Suitcases",
    "Sunglasses": "Sunglasses",
    "Suspenders": "Suspenders",
    "SwimwearBikini": "Beachwear",
    "SwimwearBikiniBottom": "Beachwear",
    "SwimwearBikiniTop": "Beachwear",
    "SwimwearOnePiece": "Beachwear",
    "SwimwearShorts": "Beachwear",
    "TVMonitors": "TVMonitors",
    "Tables": "Tables",
    "Taps": "Taps",
    "Ties": "Ties",
    "ToasterOvens": "KitchenAppliances",
    "Toasters": "KitchenAppliances",
    "Toilets": "Toilets",
    "Towels": "Towels",
    "TrashCans": "StorageOrganization",
    "Trousers": "Trousers",
    "Tubs": "Tubs",
    "UmbrellaStands": "UmbrellaStands",
    "Underwear": "Underwear",
    "Unmatched": "Fallback",
    "VacuumCleaners": "VacuumCleaners",
    "Vanities": "Vanities",
    "Vases": "Vases",
    "Vests": "CoatsJacketsSuits",
    "WaffleIrons": "KitchenAppliances",
    "WalletsPurses": "WalletsPurses",
    "WashingMachines": "HomeAppliances",
    "Watches": "Watches",
}

# CBIR configs
GPU_MEM_ALLOCATION = 2600
CBIR_BB_TO_VALID_ATTR = "bb_to_valid_attributes.json"
CBIR_BB_TO_CBIR_ML_PARENT = "bb_to_cbirmlparent.json"

COLOR_ATTR = ["Color", "ColorFamily"]

CBIR_ERROR_BINARY = "binary not in the image data keys"
CBIR_ERROR_BB_OUT_OF_BOUNDS = "the bounding box coordinates must be percentages"
CBIR_ERROR_COORDS = "No bounding box provided. Give a list under the coords key"
CBIR_ERROR_COORDS_EMPTY = "Must provide bb coordinates"
CBIR_ERROR_COORDS_FORMAT = 'Coords must be in the following format: {"x0": ..., "y0":..., "x1":..., "y1": ...} '
CBIR_ERROR_COORDS_NOT_LIST = "Coords must be a list of bounding boxes"
CBIR_ERROR_IMAGES = "images not in keys"
CBIR_ERROR_IMAGES_NOT_LIST = (
    'images have to be a list of {"b64": ..., "coords": [{"x0": ... }]}'
)

CBIR_LABEL = "label"
CBIR_LABEL_LOOKUP_STR = f"images[].coords[].{CBIR_LABEL}"
CBIR_MODEL_PATH = "model"
CBIR_SYTE_NAMES = "syte.names"

X0, Y0, X1, Y1 = "x0", "y0", "x1", "y1"

# fields - visual model
IMAGES_LIST_FIELD = "images"
IMAGE_ENCODING_FIELD = "binary"
BB_COORDS_FIELD = "coords"
REQUEST_ID_FIELD = "request_id"
FILTER_ATTRIBUTES_FIELD = "Cat"
