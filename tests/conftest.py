import pytest
import pickle
from sytedt.inferer import Prediction
from sytedt.client import InferClient, InferClientFactory


class DummyClient(InferClient):
    def __enter__(self):
        self._async = False
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self._async = None

    async def __aenter__(self):
        self._async = True
        return self

    async def __aexit__(self, exc_type, exc_val, exc_tb):
        self._async = None


class CbirDummyClient(DummyClient):
    async def predict(self, *args, **kwags) -> Prediction:
        return pickle.load(open("tests/resources/dummy_cbir_prediction.pickle", "rb"))


class YoloDummyClient(DummyClient):
    async def predict(self, *args, **kwags) -> Prediction:
        return pickle.load(open("tests/resources/dummy_yolo_prediction.pickle", "rb"))


class TagitDummyClient(DummyClient):
    async def predict(self, *args, **kwags) -> Prediction:
        return pickle.load(open("tests/resources/dummy_tagit_prediction.pickle", "rb"))


class DummyClientFactory(InferClientFactory):
    def get_client(self, network):
        if "cbir" in network:
            return CbirDummyClient()
        elif "tagit" in network:
            return TagitDummyClient()
        elif "yolo" in network:
            return YoloDummyClient()
        else:
            raise AttributeError("Cant create dummy client for %s" % network)


@pytest.fixture
def dummy_client_factory():
    return DummyClientFactory
