import os
import json
import pytest
import requests
from pathlib import Path

from vflows.context_manager import VFlowsContextManager
from vflows.models_loader import load_models
from vflows.requirements import Requirements

from sytedt.inferer import Input, InputEntity


def _get_image(image_url):
    return requests.get(image_url).content


IMG_URL_RED_DRESS = (
    "https://i.pinimg.com/736x/23/92/a4/2392a44f930f0e1046fd7084a817d4fd.jpg"
)
IMG_URL_LIVING_ROOM = "https://m.media-amazon.com/images/I/61JLG9VEm8S._SL1173_.jpg"

IMG_COUPLE_ON_KITCHEN = (
    "https://image.shutterstock.com/shutterstock/photos/674419903/display_1500/"
    "stock-photo-secret-ingredient-is-love-beautiful-young-couple-preparing-a-healthy-meal-"
    "together-while-spending-674419903.jpg"
)

MODELS_PATH = os.environ.get(
    "GRPCVFLOWS_MODELS_PATH",
    "/Users/yatagan/SRC/vmr_network_data/models",
)


async def _process(
    dummy_client_factory,
    image_url,
    catalogs,
    model_name="bb",
    model_version="2202-main",
):
    image_bin = _get_image(image_url)
    item = {"catalogs": catalogs}
    async with VFlowsContextManager(
        load_models(Path(MODELS_PATH)), Requirements(dummy_client_factory())
    ) as vflow:
        flow_input = Input(
            entities=[
                InputEntity(data=image_bin),
                InputEntity(data=json.dumps(item).encode("utf8")),
            ]
        )
        predicion = await vflow.process(model_version, model_name, flow_input)
        return predicion


@pytest.mark.asyncio
async def test_fashion(dummy_client_factory):
    prediction = await _process(
        dummy_client_factory, IMG_URL_RED_DRESS, catalogs=["fashion"]
    )
    bounds = json.loads(prediction.entities[0].data)
    assert len(bounds) != 0
